import { ports } from './config';

/**
 * Iterates through an array of port numbers attempting to execute
 * a function fn passing each port number an argument. Returns either the
 * successful port number or the error encountered.
 *
 * @param {function<Promise>} fn
 * @param {Array<number>} portNumbers
 * @returns {Promise<number|Error>}
 */
export function tryPorts(fn, portNumbers = ports) {
  try {
    const [ port, ...others ] = portNumbers;
    return fn(port)
      .then(() => port)
      .catch((e) => {
        if (others.length) {
          return tryPorts(fn, others);
        }
        return Promise.reject(e);
      });
  } catch (e) {
    // do nothing
  }
}