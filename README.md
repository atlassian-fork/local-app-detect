### Install

```js
npm i @atlassian/local-app-detect --save
```

### Usage

In the renderer process of your Electron app

```javascript
import { server } from '@atlassian/local-app-detect/server';

server
  .start()
  .then(port => {
    console.log('Started on port ', port);
  })
  .catch(e => {
    console.log('Failed to start: ', e);
  });
```

In the source to be executed in the browser context

```javascript
import { client } from '@atlassian/local-app-detect/client';

client
  .ping()
  .then(port => {
    // the app is running locally
    console.log('Server started on port ', port);
  })
  .catch(() => {
    // the app is not running
    console.log('Server not started');
  });
```