(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["main"] = factory();
	else
		root["main"] = factory();
})(window, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/client.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/client.js":
/*!***********************!*\
  !*** ./src/client.js ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\nexports.client = undefined;\n\nvar _utils = __webpack_require__(/*! ./common/utils */ \"./src/common/utils.js\");\n\n/**\n * Requests an image resource hosted locally at a specific port number\n *\n * @param {number} port\n * @returns {Promise}\n */\nfunction pingServer(port) {\n  return new Promise(function (resolve, reject) {\n    var image = new Image();\n    var url = 'http://127.0.0.1:' + port;\n    image.onload = resolve;\n    image.onerror = reject;\n    image.src = url;\n  });\n}\n\nfunction ping(ports) {\n  return (0, _utils.tryPorts)(pingServer, ports);\n}\n\nvar client = exports.client = { ping: ping };\n\n//# sourceURL=webpack://%5Bname%5D/./src/client.js?");

/***/ }),

/***/ "./src/common/config.js":
/*!******************************!*\
  !*** ./src/common/config.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\nvar ports = exports.ports = Object.freeze([22274, 22300, 22304, 31417, 31456]);\n\n//# sourceURL=webpack://%5Bname%5D/./src/common/config.js?");

/***/ }),

/***/ "./src/common/utils.js":
/*!*****************************!*\
  !*** ./src/common/utils.js ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\nexports.tryPorts = tryPorts;\n\nvar _config = __webpack_require__(/*! ./config */ \"./src/common/config.js\");\n\nfunction _toArray(arr) { return Array.isArray(arr) ? arr : Array.from(arr); }\n\n/**\n * Iterates through an array of port numbers attempting to execute\n * a function fn passing each port number an argument. Returns either the\n * successful port number or the error encountered.\n *\n * @param {function<Promise>} fn\n * @param {Array<number>} portNumbers\n * @returns {Promise<number|Error>}\n */\nfunction tryPorts(fn) {\n  var portNumbers = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : _config.ports;\n\n  try {\n    var _portNumbers = _toArray(portNumbers),\n        port = _portNumbers[0],\n        others = _portNumbers.slice(1);\n\n    return fn(port).then(function () {\n      return port;\n    }).catch(function (e) {\n      if (others.length) {\n        return tryPorts(fn, others);\n      }\n      return Promise.reject(e);\n    });\n  } catch (e) {\n    // do nothing\n  }\n}\n\n//# sourceURL=webpack://%5Bname%5D/./src/common/utils.js?");

/***/ })

/******/ });
});